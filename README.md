Homepage of [joinpeertube.org](https://joinpeertube.org) based on [Hugo](https://gohugo.io/) and the [Hugo Bootstrap Premium](https://themes.gohugo.io/hugo-bootstrap-premium/) theme.

- texts are in the `content` folder.
- navbar content is defined in `config.toml`
- CSS are in `themes/hugo-bootstrap-premium/static/css/style.css`
- images are in `static`

To see your changes in action, run :

    hugo server -D -b "http://localhost:1313/"

